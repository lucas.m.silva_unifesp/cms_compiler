/*************************/
/* Arquivo: analyse.c    */
/* Lucas Moura da Silva  */
/*************************/

#include "globals.h"
#include "symtab.h"
#include "analyze.h"

/*
    Procedimento insertNode insere 
    identidicadores armazenados na
    arvore sintatica para a tabela 
    de simbolos
*/
static void insertNode(TreeNode * t, EscopeLis * e)
{
    if (t->nodekind == StmtK) {
        switch (t->kind.stmt) {
            case VariableK:   
                // caso ainda nao tenha na tabela, entao trata como definicao nova
                if (st_lookup(e, t->attr.name) == -1)
                    st_insert(e, t->attr.name, t->lineno, "Integer", "Variable");
                else
                // caso ja tenha na tabela, entao ignorar localizacao e apenas acresentar numero da linha
                    st_insert(e, t->attr.name, t->lineno, "Integer", "Variable");
                break;
            case FunctionK:   
                // caso ainda nao tenha na tabela, entao trata como definicao nova
                if (st_lookup(e, t->attr.name) == -1)
                    st_insert(e, t->attr.name, t->lineno, t->type==Integer?"Integer":"Void", "Function");
                else
                // caso ja tenha na tabela, entao ignorar localizacao e apenas acresentar numero da linha
                    st_insert(e, t->attr.name, t->lineno, t->type==Integer?"Integer":"Void", "Function");
                break;
            case CallK:       
                // caso ainda nao tenha na tabela, entao trata como definicao nova
                if (st_lookup(e, t->attr.name) == -1)
                    st_insert(e, t->attr.name, t->lineno, "-", "Call");
                else
                // caso ja tenha na tabela, entao ignorar localizacao e apenas acresentar numero da linha
                    st_insert(e, t->attr.name, t->lineno, "-", "Call");
                break;
            default:          
                break;
        }
    } else {
        switch (t->kind.exp) {
            case IdK:
                // caso ainda nao tenha na tabela, entao trata como definicao nova
                if (st_lookup(e, t->attr.name) == -1)
                    st_insert(e, t->attr.name, t->lineno, "Integer", "Identifier");
                else
                // caso ja tenha na tabela, entao ignorar localizacao e apenas acresentar numero da linha
                    st_insert(e, t->attr.name, t->lineno, "Integer", "Identifier");
                break;
            case VectorK:
                // caso ainda nao tenha na tabela, entao trata como definicao nova
                if (st_lookup(e, t->attr.name) == -1)
                    st_insert(e, t->attr.name, t->lineno, "Integer", "Vector");
                else
                // caso ja tenha na tabela, entao ignorar localizacao e apenas acresentar numero da linha
                    st_insert(e, t->attr.name, t->lineno, "Integer", "Vector");
                break;
            default:
                break;
        }
    }
}

/*
    Procedimento para a percursao da arvore sintatica
    de forma pre-ordem, alem de criar as tabelas de 
    simbolos adicionais para cada escopo
*/
static void insert(TreeNode * t, EscopeLis * e)
{
    if (t != NULL) {
        insertNode(t, e);

        int i;

        if (t->nodekind == StmtK && t->kind.stmt == FunctionK) {
            EscopeLis * ei; // escopo interno da funcao
            ei = buildEscopeList(t->attr.name, e);

            if (e->child == NULL)
                e->child = ei;
            else {
                EscopeLis * sibling;
                sibling = e->child;

                while (sibling->sibling != NULL)
                    sibling = sibling->sibling;
                
                sibling->sibling = ei;
            }

            for (i=0; i<MAXCHILDREN; i++)
                insert(t->child[i], ei);
        } else {
            for (i=0; i<MAXCHILDREN; i++)
                insert(t->child[i], e);
        }

        insert(t->sibling, e);
    }

}


void buildSymtab(TreeNode * syntaxTree)
{
    EscopeLis * global;
    global = buildEscopeList("global", NULL);

    insert(syntaxTree, global);

    if (TraceAnalyze) {
        fprintf(listing, "\nTabela de simbolos:\n\n");
        printSymtab(global, listing);
    }
}

/*

*/
static void typeError(TreeNode * t, char * message)
{
    fprintf(listing, "Type error at line %d: %s\n", t->lineno, message);
    Error = TRUE;
}

/*

*/
static void checkNode(TreeNode * t)
{
    if (t->nodekind == StmtK) {
        switch (t->kind.stmt) {
            case IfK:
                if (t->child[0]->type != Boolean)
                    typeError(t->child[0], "If test is not Boolean");
                break;
            case WhileK:
                if (t->child[0]->type != Boolean)
                    typeError(t->child[0], "While test is not Boolean");
                break;
            case AssignK:
                if ((t->child[0]->type != Integer) || (t->child[1]->type != Integer))
                    typeError(t, "Assign test is not Integer");
                break;
            case VariableK:
                if (t->child[0]->type == Integer)
                    t->type = Integer;
                else 
                    typeError(t, "Variable test is not integer");
                break;
            case FunctionK:
                if (t->child[0]->type == Void)
                    t->type = Void;
                else if (t->child[0]->type == Integer)
                    t->type = Integer;
                else 
                    typeError(t, "Function test is not integer or void");
                break;
            case CallK:  
                break;
            case ReturnK:
                if (t->child[0] != NULL) {
                    if (t->child[0]->type == Integer)
                        t->type = Integer;
                    else 
                        typeError(t, "Return applied to non-integer");
                } else
                    t->type = Void;
                break;
            default:          
                break;
        }
    } else {
        switch (t->kind.exp) {
            case OpK:
                if ((t->attr.op == ST) || (t->attr.op == STE) || (t->attr.op == BT) || (t->attr.op == BTE) || (t->attr.op == EQ) || (t->attr.op == NE))
                    t->type = Boolean;
                else
                    t->type = Integer;
                break;
            case ConstK:
                t->type = Integer;
                break;
            case IdK:
                t->type = Integer;
                break;
            case VectorK:
                t->type = Integer;
                break;
            case IndexVector:
                t->type = Integer;
                break;
            case TypeK:
                if (strcmp(t->attr.name, "integer"))
                    t->type = Void;
                else
                    t->type = Integer;
                break;
            default:
                break;
        }
    }
}

/*

*/
static void check(TreeNode * t)
{
    if (t != NULL) {
        int i;

        for (i=0; i<MAXCHILDREN; i++)
            check(t->child[i]);

        checkNode(t);

        check(t->sibling);
    }
}

void typeCheck(TreeNode * syntaxTree)
{
    check(syntaxTree);
    // depois colocar no main o procedimento typecheck antes de buildSimtam
}