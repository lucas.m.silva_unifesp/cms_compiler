/*************************/
/* Arquivo: util.h       */
/* Lucas Moura da Silva  */
/*************************/

#ifndef _UTIL_H_
#define _UTIL_H_

/* 
    Procedimento printToken imprime um marcador
    e seu lexema no arquivo de listagem
 */
void printToken(TokenType, const char*);

/* 
    Funcao newStmtNode cria um novo no de declaracao para a
    construcao da arvore sintatica
 */
TreeNode * newStmtNode(StmtKind);

/* 
    Funcao newStmtNode cria um novo no de expressao para a
    construcao da arvore sintatica
 */
TreeNode * newExpNode(ExpKind);

/* 
    Funcao copyString aloca e cria nova
    copia de uma cadeia existente
 */
char * copyString(char *);

/* 
    procesimento printTree imprime uma arvore sintatica no
    arquivo de listagem usado tabulacao para indicar subarvores
 */
void printTree(TreeNode *);

#endif