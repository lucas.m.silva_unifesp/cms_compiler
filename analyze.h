/*************************/
/* Arquivo: analyse.h    */
/* Lucas Moura da Silva  */
/*************************/

#ifndef _ANALYZE_H_
#define _ANALYZE_H_

/*
    Funcao buildSymtab constroi a tabela de simbolos
    por percurso em pre-ordem da arovre sintatica
*/
void buildSymtab(TreeNode *);

/*
    Procedimento typeCheck efetua verificacao de tipos
    por percurso em pos-ordem da arvore sintatica
*/
void typeCheck(TreeNode *);

#endif