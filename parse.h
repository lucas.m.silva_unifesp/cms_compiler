/*************************/
/* Arquivo: parse.h      */
/* Lucas Moura da Silva  */
/*************************/

#ifndef _PARSE_H_
#define _PARSE_H_

/*
    Funcao parse retorna a
    arvore sintatica nova construida
*/

TreeNode * parse(void);

#endif