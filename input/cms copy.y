/****************************************/
/* File: cminus.y                       */
/* The C- Yacc/Bison specification file */
/****************************************/

%{
#define YYPARSER
#include "globals.h"
#include "util.h"
#include "scan.h"
#include "parse.h"

#define YYSTYPE TreeNode *
static YYSTYPE savedTree;  /* armazena arvore sintatica para reorno posterior*/
static char * savedName;      /* para uso em atribuicoes */
static int savedLineNo;       /* idem */
static int yylex(void);
%}

%token IF ELSE INT VOID RETURN WHILE
%token ID NUM 
%token PLUS MINUS TIMES OVER ST STE BT BTE EQ NE ASSIGN SEMI COMMA LPAREN RPAREN LBRACKET RBRACKET LBRACES RBRACES
%token ERROR ENDFILE

%% /* Grammar for C- */
programa                : declaracao_lista
                          {
                            savedTree = $1;
                          }
                        ;
declaracao_lista        : declaracao_lista declaracao
                          {
                            YYSTYPE t = $1;
                            if (t != NULL) {
                              while (t->sibling != NULL)
                                t = t->sibling;
                              t->sibling = $2;
                              $$ = $1;
                            } 
                            else 
                              $$ = $2;
                          }
                        | declaracao
                          {
                            $$ = $1;
                          }
                        ;
declaracao              : var_declaracao
                          {
                            $$ = $1;
                          }
                        | fun_declaraco
                          {
                            $$ = $1;
                          }
                        ;
var_declaracao          : tipo_especificador ident SEMI
                          {
                            $$ = $1;
                            $2->nodekind = StmtK;
                            $2->kind.stmt = VariableK;
                            $2->type = $1->type;
                            $1->child[0] = $2;
                          }
                        | tipo_especificador ident LBRACKET num RBRACKET SEMI
                          {
                            $$ = $1;
                            $2->nodekind = StmtK;
                            $2->kind.stmt = VariableK;
                            $2->type = $1->type;
                            $1->child[0] = $2;
                            $2->attr.len = $4->attr.val;
                          }
                        ;
tipo_especificador      : INT
                          {
                            $$ = newExpNode(TypeK);
                            $$->type = Integer;
                            $$->attr.name = "integer";
                          }
                        | VOID
                          {
                            $$ = newExpNode(TypeK);
                            $$->attr.name  = "void";
                          }
                        ;
fun_declaraco           : tipo_especificador ident LPAREN param_lista RPAREN composto_decl
                          {
                            $$ = $1;
                            $2->nodekind = StmtK;
                            $2->kind.stmt = FunctionK;
                            $2->type = $1->type;
                            $$->child[0] = $2;
                            $2->child[0] = $4;
                            $2->child[1] = $6;

                            attScope($2->child[0], $2->attr.name);
                            attScope($2->child[1], $2->attr.name);
                          }
                        ;
param_lista             : param_lista COMMA param
                          {
                            YYSTYPE t = $1;
                            if (t != NULL) {
                              while (t->sibling != NULL)
                                t = t->sibling;
                              t->sibling = $3;
                              $$ = $1;
                            } 
                            else 
                              $$ = $3;
                          }
                        | param
                          {
                            $$ = $1;
                          }
                        | VOID
                          {
                            $$ = newExpNode(TypeK);
                            $$->attr.name  = "void";
                          }
                        ;
param                   : tipo_especificador ident 
                          {
                            $$ = $1;
                            $2->nodekind = StmtK;
                            $2->kind.stmt = VariableK;
                            $2->type = $1->type;
                            $1->child[0] = $2;
                          }
                        | tipo_especificador ident LBRACKET RBRACKET
                          {
                            $$ = $1;
                            $2->nodekind = StmtK;
                            $2->kind.stmt = VariableK;
                            $2->type = $1->type;
                            $1->child[0] = $2;
                          }
                        ;
composto_decl           : LBRACES local_declaracoes statement_lista RBRACES
                          {
                            YYSTYPE t = $2;
                            if (t != NULL) {
                              while (t->sibling != NULL)
                                t = t->sibling;
                              t->sibling = $3;
                              $$ = $2;
                            } 
                            else 
                              $$ = $3;
                          }
                        ;
local_declaracoes       : local_declaracoes var_declaracao
                          {
                            YYSTYPE t = $1;
                            if (t != NULL) {
                              while (t->sibling != NULL)
                                t = t->sibling;
                              t->sibling = $2;
                              $$ = $1;
                            } 
                            else 
                              $$ = $2;
                          }
                        | /* vazio */
                          {
                            $$ = NULL;
                          }
                        ;
statement_lista         : statement_lista statement
                          {
                            YYSTYPE t = $1;
                            if (t != NULL) {
                              while (t->sibling != NULL)
                                t = t->sibling;
                              t->sibling = $2;
                              $$ = $1;
                            } 
                            else 
                              $$ = $2;
                          }
                        | /* vazio */               
                          {
                            $$ = NULL;
                          }        
                        ;
statement               : expressao_decl
                          {
                            $$ = $1;
                          }
                        | composto_decl
                          {
                            $$ = $1;
                          }
                        | selecao_decl
                          {
                            $$ = $1;
                          }
                        | iteracao_decl
                          {
                            $$ = $1;
                          }
                        | retorno_decl
                          {
                            $$ = $1;
                          }
                        ;          
expressao_decl          : expressao SEMI
                          {
                            $$ = $1;
                          }
                        | SEMI
                          {
                            $$ = NULL;
                          }
                        ;
selecao_decl            : IF LPAREN expressao RPAREN statement
                          {
                            $$ = newStmtNode(IfK);
                            $$->child[0] = $3;
                            $$->child[1] = $5;
                          }
                        | IF LPAREN expressao RPAREN statement ELSE statement
                          {
                            $$ = newStmtNode(IfK);
                            $$->child[0] = $3;
                            $$->child[1] = $5;
                            $$->child[2] = $7;
                          }
                        ;
iteracao_decl           : WHILE LPAREN expressao RPAREN statement
                          {
                            $$ = newStmtNode(WhileK);
                            $$->child[0] = $3;
                            $$->child[1] = $5;
                          }
                        ;
retorno_decl            : RETURN SEMI
                          {
                            $$ = newStmtNode(ReturnK);
                          }
                        | RETURN expressao SEMI
                          {
                            $$ = newStmtNode(ReturnK);
                            $$->child[0] = $2;
                          }
                        ;
expressao               : var ASSIGN expressao
                          {
                            $$ = newStmtNode(AssignK);
                            $$->attr.op = ASSIGN;
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                          }
                        | simples_expressao
                          {
                            $$ = $1;
                          }
                        ;
var                     : ident
                          {
                            $$ = $1;
                          }
                        | ident LBRACKET expressao RBRACKET
                          {
                            $$ = $1;
                            $$->child[0] = $3;
                            $$->kind.exp = VectorK;
                            $$->type = Integer;
                          }
                        ;
simples_expressao       : soma_expressao relacional soma_expressao
                          {
                            $$ = $2;
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                          }
                        | soma_expressao
                          {
                            $$ = $1;
                          }
                        ;
relacional              : STE
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = STE;
                            $$->type = Boolean;
                          }
                        | ST
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = ST;
                            $$->type = Boolean;
                          }
                        | BT
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = BT;
                            $$->type = Boolean;
                          }
                        | BTE
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = BTE;
                            $$->type = Boolean;
                          }
                        | EQ
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = EQ;
                            $$->type = Boolean;
                          }
                        | NE
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = NE;
                            $$->type = Boolean;
                          }
                        ;
soma_expressao          : soma_expressao soma termo
                          {
                            $$ = $2;
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                          }
                        | termo
                          {
                            $$ = $1;
                          }
                        ;
soma                    : PLUS
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = PLUS;
                          }
                        | MINUS
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = MINUS;
                          }
                        ;
termo                   : termo mult fator
                          {
                            $$ = $2;
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                          }
                        | fator
                          {
                            $$ = $1;
                          }
                        ;
mult                    : TIMES
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = TIMES;
                          }
                        | OVER
                          {
                            $$ = newExpNode(OpK);
                            $$->attr.op = OVER;
                          }
                        ;
fator                   : LPAREN expressao RPAREN
                          {
                            $$ = $2;
                          }
                        | var
                          {
                            $$ = $1;
                          }
                        | ativacao
                          {
                            $$ = $1;
                          }
                        | num
                          {
                            $$ = $1;
                          }
                        ;
ativacao                : ident LPAREN args RPAREN
                          {
                            $$ = $1;
                            $$->child[0] = $3;
                            $$->nodekind = StmtK;
                            $$->kind.stmt = CallK;
                          }
                        ;
args                    : arg_lista
                          {
                            $$ = $1;
                          }
                        | /* vazio */
                          {
                            $$ = NULL;
                          }
                        ;
arg_lista               : arg_lista COMMA expressao
                          {
                            YYSTYPE t = $1;
                            if (t != NULL) {
                              while (t->sibling != NULL)
                                t = t->sibling;
                              t->sibling = $3;
                              $$ = $1;
                            } 
                            else 
                              $$ = $3;
                          }
                        | expressao
                          {
                            $$ = $1;
                          }
                        ;
ident                   : ID
                          {
                            $$ = newExpNode(IdK);
                            $$->attr.name = copyString(tokenString);
                          }
                        ;
num                     : NUM
                          {
                            $$ = newExpNode(ConstK);
                            $$->attr.val = atoi(tokenString);
                            $$->type = Integer;
                          }
                        ;                        


%%

int yyerror(char * message)
{ 
  fprintf(listing,"ERRO SINTÁTICO: %s\n", message);
  fprintf(listing,"TOKEN: ");
  printToken(yychar,tokenString);
  fprintf(listing,"LINHA: %i\n", lineno);
  Error = TRUE;
  return 0;
}

static int yylex(void)
{
  return getToken();
}

TreeNode * parse(void)
{
  yyparse();
  return savedTree;
}
