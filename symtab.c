/*************************/
/* Arquivo: symtab.c     */
/* Lucas Moura da Silva  */
/*************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "symtab.h"

/*
    a funcao de hashing
*/
static int hash(char * key)
{
    int temp = 0;
    int i = 0;
    
    while (key[i] != '\0') {
        temp = ((temp << SHIFT) + key[i]) % SIZE;
        ++i;
    }
    return temp;
}

EscopeLis * buildEscopeList(char * escope, EscopeLis * prev)
{
    EscopeLis * e;

    e = (EscopeLis *) malloc(sizeof(struct EscopeListRec));
    e->escope = escope;
    e->prev = prev;
    e->child = NULL;
    e->sibling = NULL;

    return e;
}

void st_insert(EscopeLis * e, char * name, int lineno, char * type, char * nodeType)
{
    int h = hash(name);

    BucketList l = e->hashTable[h];

    while ((l != NULL) && (strcmp(name, l->name) != 0))
        l = l->next;
    
    if (l == NULL) { // variavel ainda nao na tabela
        l = (BucketList) malloc(sizeof(struct BucketListRec));
        l->name = name;
        l->lines = (LineList) malloc(sizeof(struct LineListRec));
        l->lines->lineno = lineno;
        l->lines->next = NULL;
        // l->memloc = loc;
        l->type = type;
        l->nodeType = nodeType;
        l->next = e->hashTable[h];
        e->hashTable[h] = l;
    } else { // encontrada na tabela, entao acrescente numero de linha
        LineList t = l->lines;

        while (t->next != NULL)
            t = t->next;

        t->next = (LineList) malloc(sizeof(struct LineListRec));
        t->next->lineno = lineno;
        t->next->next = NULL;
    }
}

int st_lookup(EscopeLis * e, char * name)
{
    int h = hash(name);

    BucketList l = e->hashTable[h];

    while ((l != NULL) && (strcmp(name, l->name) != 0))
        l = l->next;

    if (l == NULL)
        return -1;
    
    else
        // return l->memloc;
        return 1;
}

void printSymtab(EscopeLis * e, FILE * listing)
{
    EscopeLis * escope;
    escope = e;

    int i;

    fprintf(listing, "Escope    Variable Name   Type      Node Type    Line Numbers\n");
    fprintf(listing, "- - -     - - - - - - -   - - - -   - - - - -    - - - - - -\n");

    while (escope != NULL) {
        for (i=0; i<SIZE; ++i) {
            if (escope->hashTable[i] != NULL) {
                BucketList l = escope->hashTable[i];

                while (l != NULL) {
                    LineList t = l->lines;
                    
                    fprintf(listing, "%-10s", escope->escope);
                    fprintf(listing, "%-16s", l->name);
                    fprintf(listing, "%-10s", l->type);
                    fprintf(listing, "%-13s", l->nodeType);
                    // fprintf(listing, "%-8d              ", l->memloc);

                    while (t != NULL) {
                        fprintf(listing, "%-4d", t->lineno);
                        t = t->next;
                    }

                    fprintf(listing, "\n");

                    l = l->next;
                }
            }
        }

        if (escope->child != NULL)
            escope = escope->child;
        else if (escope->sibling != NULL)
            escope = escope->sibling;
        else 
            escope = NULL;
    }
}