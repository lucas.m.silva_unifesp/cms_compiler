/*************************/
/* Arquivo: scan.h       */
/* Lucas Moura da Silva  */
/*************************/

#include "globals.h"

#ifndef _scan_h_
#define _scan_h_

/* 
    MAXTOKENLEN eh o tamanho maximo de um marcador 
*/
#define MAXTOKENLEN 40

/* 
    matriz tokenString armazena o lexema de cada marcador
*/
extern char tokenString[MAXTOKENLEN+1];

/* 
    funcao getToken retorna o marcador
    seguinte no arquivo-fonte
 */
TokenType getToken(void);

#endif