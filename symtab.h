/*************************/
/* Arquivo: symtab.h     */
/* Lucas Moura da Silva  */
/*************************/

#ifndef _SYMTAB_H_
#define _SYMTAB_H_

/*
    SIZE eh o tamanho da tabela de hashing
*/
#define SIZE 101

/*
    SHIFT eh a potencia de dois usada como multiplicador
    na funcao de hashing 
*/
#define SHIFT 4

/*
    a lista de numeros de linhas no codigo-fonte
    em que uma variavel eh referenciada
*/
typedef struct LineListRec
{
    int lineno;
    struct LineListRec * next;
} * LineList;

/*
    O registro nas listas de repositorios para
    cada variavel dentro do esocpo, incluindo o nome,
    localizacao atribuida de memoria e 
    a lista de numeros de linhas em que
    ela aparece no codigo-fonte
*/
typedef struct BucketListRec
{
    char * name;
    LineList lines;
    int  memloc; // localizacao de memoria para variavel;
    char * type;
    char * nodeType;
    struct BucketListRec * next;
} * BucketList;

/*
    O registro das tabelas de simbolos
    para cada escopo
*/
typedef struct EscopeListRec
{
    char * escope;
    BucketList hashTable[SIZE];
    struct EscopeListRec * prev; // ponteiro para o pai
    struct EscopeListRec * child;
    struct EscopeListRec * sibling;
} EscopeLis;

EscopeLis * buildEscopeList(char * escope, EscopeLis * prev);

/*
    Procedimento st_insert insere enumeracao de linhas e
    localizacoes de memoria na tabela de simbolos
    loc = localizacao de memoria eh inserida apenas na 
    primeira vez, caso contrario eh ignorada
*/
void st_insert(EscopeLis * e, char * name, int lineno, char * type, char * nodeType);

void st_remove(char * name);

/*
    Funcao st_lookup retrna a localizacao 
    na memoria de uma variavel ou -1 se nao encontrar
*/
int st_lookup(EscopeLis * e, char * name);

/*
    Procedimento printSymtab imprime uma
    listagem formatada da tabela de simbolos no
    arquivo de listagem
*/
void printSymtab(EscopeLis * e, FILE * listing);

#endif