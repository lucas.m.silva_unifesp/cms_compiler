/*****************************/
/* Especificacao Lex para C- */
/* Lucas Moura da Silva      */
/*****************************/

%{
#include "globals.h"
#include "util.h"
#include "scan.h"

/* lexema do identificador ou palavra reservada */
char tokenString[MAXTOKENLEN+1];

static int yylex(void);
%}

%option noyywrap

digit           [0-9]
number          {digit}+
small_letter    [a-z]
capital_letter  [A-Z]
letter          {small_letter}|{capital_letter}
identifier      {letter}+
newline         \n
whitespace      [ \t]+

%%

"/*"            { char c;
                  do
                  { c = input();
                    if (c == EOF) break;
                    if (c == '\n') lineno++;
  		            if (c == '*') 
                    { 
		                if(input() == '/')
		                break;
		            }		
                  } while (1);
                }

"else"          {return ELSE;}
"if"            {return IF;}
"int"           {return INT;}
"return"        {return RETURN;}
"void"          {return VOID;}
"while"         {return WHILE;}
"+"             {return PLUS;}
"-"             {return MINUS;}
"*"             {return TIMES;}
"/"             {return OVER;}
"<"             {return ST;}        // smaller than
"<="            {return STE;}       // smaller than equal
">"             {return BT;}        // bigger than
">="            {return BTE;}       // bigger than equal
"=="            {return EQ;}        // equal
"!="            {return NE;}        // not equal
"="             {return ASSIGN;}    // atribuicao
";"             {return SEMI;}
","             {return COMMA;}
"("             {return LPAREN;}
")"             {return RPAREN;}
"["             {return LBRACKET;}  // left bracket
"]"             {return RBRACKET;}  // right bracket
"{"             {return LBRACES;}   // left braces
"}"             {return RBRACES;}   // right braces
{number}        {return NUM;}
{identifier}    {return ID;}
{newline}       {lineno++;}
{whitespace}    {/* nada */}
.               {return ERROR;}

%%

TokenType getToken(void)
{
  static int firstTime = TRUE;
  TokenType currentToken;

  if (firstTime)
  {
    firstTime = FALSE;
    lineno++;
    yyin = source;
    yyout = listing;
  }

  currentToken = yylex();
  strncpy(tokenString, yytext, MAXTOKENLEN);

  if (TraceScan)
  {
    fprintf(listing, "\t%d: ", lineno);
    printToken(currentToken, tokenString);
  }

  return currentToken;
}