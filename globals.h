/*************************/
/* Arquivo: globals.h    */
/* Lucas Moura da Silva  */
/*************************/


#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

/*
    Yacc/Bison gera internamente seus proprios valores
    para os marcadores. Outros arquivos podem acessar 
    esses valores incluindo o arquivo tab.h gerado usando a 
    opcao Yacc/Bison -d ("gera cabecalho")
    A marca YYPARSER impede a inclusao de tab.h
    na saida Yacc/Bison

*/

#ifndef YYPARSER

#include "cms.tab.h"

/*
    ENDFILE eh definido implicitamente por Yacc/Bison
    e nao eh incluido arquivo tab.h
*/
#define ENDFILE 0

#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

/* MAXRESERVED = quantidade de palavras reservadas */
#define MAXRESERVED 6

/*
    Yacc/Bison gera seus proporios valores inteios
    para marcadores
*/
typedef int TokenType;

extern FILE* source;    /* arquivo texto de codigo-fonte */
extern FILE* listing;   /* arquivo texto de listagem de saida */
extern FILE* code;      /* arquivo texto de codigo para simulador TM */

extern int lineno; /* Arvore sintatica para analise sintatica */


/*******************************************************************/
/*********** Arvore sintatica para a analise sintatica  ************/
/*******************************************************************/


typedef enum
{
	StmtK, ExpK 
} NodeKind;

typedef enum
{
	IfK, WhileK, AssignK, VariableK, FunctionK, CallK, ReturnK
} StmtKind;

typedef enum
{
	OpK, ConstK, IdK, VectorK, IndexVector, TypeK
} ExpKind;

/* ExpType eh usado para verificacao de tipos */
typedef enum
{
	Void, Integer, Boolean
} ExpType;


#define MAXCHILDREN 3


typedef struct treeNode
{ 
    struct treeNode * child[MAXCHILDREN];
    struct treeNode * sibling;
    int lineno;
    NodeKind nodekind;

    union 
    { 
        StmtKind stmt; 
        ExpKind exp;
    } kind;

    struct 
    { 
        TokenType op;
        int val;
        int len;
        char* name; 
        char* scope;
    } attr;

    ExpType type; /* Marcas para acompanhamento de execucao */
} TreeNode;


/*******************************************************************/
/*********** Marcadores para acompanhamento de execucao ************/
/*******************************************************************/

/*
    ExhoSource = TRUE ecoa o programa-fonte no
    arquivo de listagem com enumeracao de linhas durante a 
    analise sintatica
*/
extern int EchoSource;

/*
    TraceScan = TRUE imprime informacoes de
    marcadores no arquivo de listagem durante o 
    reconhecimento de cada marcador pelo sistema de varredura
*/
extern int TraceScan;

/*
    TraceParse = TRUE imprime a arvore sintativa no
    arquivo de listagem em forma linearizada
    (com identificadores para filhos)
*/
extern int TraceParse;

/*
    TraceAnalyze = TRUE registra no arquivo de listagem
    insercoes e buscas na tabela de simbolos
*/
extern int TraceAnalyze;

/*
    TraceCode = TRUE escreve comentarios no arquivo de
    codigo TM durante a geracao de codigo
*/
extern int TraceCode;

/*
    Error = TRUE impede passadas posteriores se ocerrer um erro
*/
extern int Error; 
#endif
